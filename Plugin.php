<?php namespace Boarworm\EasySeo;

use Backend;
use Event;
use Yaml;
use System\Classes\PluginManager;
use System\Classes\PluginBase;
use Boarworm\EasySeo\Models\Settings;
use Boarworm\EasySeo\Models\Meta;

/**
 * EasySeo Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'boarworm.easyseo::lang.plugin.name',
            'description' => 'boarworm.easyseo::lang.plugin.description',
            'author' => 'Boarworm',
            'icon' => 'icon-magic'
        ];
    }

    public function boot()
    {
        $this->createCmsPagesFields();
        $this->createCustomModelFields();
        $this->createStaticPagesFields();
    }

    public function createStaticPagesFields()
    {
        if (PluginManager::instance()->hasPlugin('RainLab.Pages')) {
            Event::listen('backend.form.extendFields', function ($widget) {
                if ($widget->isNested || !$widget->model instanceof \RainLab\Pages\Classes\Page) return;
                $widget->addFields($this->getStaticPageFields(), 'primary');
            });
        }
    }

    public function createCmsPagesFields()
    {
        // if ocms v2-3
        if (class_exists('System')) {
            \Event::listen('cms.template.extendTemplateSettingsFields', function ($extension, $dataHolder) {
                if ($dataHolder->templateType === 'page') {
                    foreach ($this->getCmsPageFields() as $setting) {
                        $dataHolder->settings[] = $setting;
                    }
                }
            });
        } else {
            \Event::listen('backend.form.extendFields', function ($widget) {
                if (!$widget->getController() instanceof \Editor\Controllers\Index || !$widget->model instanceof \Editor\Classes\Page || $widget->isNested) return;
                $widget->addTabFields($this->getCmsPageFields());
            });
        }
    }

    public function createCustomModelFields()
    {
        if ($metaRelations = Settings::instance()->relations) {
            foreach ($metaRelations as $metaRelation) {
                $item_model      = $metaRelation['item_model'];
                $item_controller = $metaRelation['item_controller'];
                $tab_type        = $metaRelation['tab_type'];

                // add relationship
                if (class_exists($item_model)) {
                    $item_model::extend(function ($model) {
                        $model->morphOne['meta'] = [
                            Meta::class,
                            'name' => 'taggable',
                        ];
                    });
                }

                // add fields
                Event::listen('backend.form.extendFields', function ($widget) use ($item_model, $tab_type) {
                    if (!$widget->model instanceof $item_model || $widget->isNested) {
                        return;
                    }
                    $widget->{$tab_type}($this->getCustomPageFields());
                });

                // check for meta exists
                if (class_exists($item_controller)) {
                    $item_controller::extendFormFields(function ($widget, $model, $context) use ($item_model) {
                        if (!$model instanceof $item_model) return;
                        if (!$model->meta) {
                            $model->meta = new Meta;
                        }
                    });
                }
            }
        }
    }

    public function getStaticPageFields()
    {
        $fieldsConfig = Yaml::parseFile(plugins_path() . '/boarworm/easyseo/config/fields.yaml');
        foreach ($fieldsConfig as $name => $value) {
            $fields['viewBag[' . $name . ']'] = $value;
        }
        return $fields;
    }

    public function getCustomPageFields()
    {
        $fieldsConfig = Yaml::parseFile(plugins_path() . '/boarworm/easyseo/config/fields.yaml');
        foreach ($fieldsConfig as $name => $value) {
            $fields['meta[' . $name . ']'] = $value;
        }
        return $fields;
    }

    public function getCmsPageFields()
    {
        // if ocms v2-3
        if (class_exists('System')) {
            $fields = [
                [
                    'property' => 'meta_keywords',
                    'title' => 'cms::lang.editor.meta_title',
                    'tab' => 'cms::lang.editor.meta',
                    'type' => 'string',
                    'attributes' => [
                        'maxlength' => '1000'
                    ]
                ]
            ];
        } else {
            $fieldsConfig = Yaml::parseFile(plugins_path() . '/boarworm/easyseo/config/fields.yaml');
            foreach ($fieldsConfig as $name => $value) {
                $fields['viewBag[' . $name . ']'] = $value;
            }
        }
        return $fields;
    }

    public function registerSettings()
    {
        return [
            'easyseo' => [
                'label' => 'boarworm.easyseo::lang.settings.label',
                'description' => 'boarworm.easyseo::lang.settings.description',
                'category' => 'boarworm.easyseo::lang.settings.category',
                'icon' => 'icon-globe',
                'class' => 'Boarworm\EasySeo\Models\Settings',
                'order' => 500,
                'permissions' => ['boarworm.easyseo.manage_seo_settings'],
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'Boarworm\EasySeo\Components\EasySeo' => 'EasySeo',
        ];
    }

    public function registerPermissions()
    {
        return [
            'boarworm.easyseo.manage_seo_settings' => [
                'tab' => 'boarworm.easyseo::lang.permission.tab',
                'label' => 'boarworm.easyseo::lang.permission.label'
            ],
        ];
    }
}
