<?php return [
    'plugin' => [
        'name' => 'EasySeo',
        'description' => 'Простой SEO плагин который работает с CMS страницами, Статическими страницами и пользовательскими моделями.',
    ],
    'field' => [
        'meta_title' => 'Meta Title',
        'meta_title_placeholder' => 'Около 60 символов для Google',
        'meta_description' => 'Meta Description',
        'meta_description_placeholder' => 'Около 120-150 символов для Google',
        'meta_keywords' => 'Meta Keywords',
        'meta_keywords_placeholder' => 'Нет официального лимита',
        'cms_pages' => 'CMS страницы',
        'static_pages' => 'Static страницы',
        'custom_pages' => 'Пользовательские страницы',
        'page' => 'Страница',
        'param_label' => 'URL Параметр',
        'param_description' => 'Название параметра',
        'empty_option' => '-- Пожалуйста выберите --',
        'empty_field' => '-- Пожалуйста введите --',
        'tab_type' => 'Тип вкладки',
        'path_to_model_label' => 'Путь к модели',
        'path_to_model_description' => 'Например: RainLab\Blog\Models\Post',
        'path_to_controller_label' => 'Путь к контроллеру',
        'path_to_controller_description' => 'Например: RainLab\Blog\Controllers\Posts',
        'default_title_label' => 'Title по умолчанию',
        'default_title_description' => 'Этот шаблон будет использован по умолчанию (если поле не заполнено)',
        'default_description_label' => 'Description по умолчанию',
        'default_description_description' => 'Этот шаблон будет использован по умолчанию (если поле не заполнено)',
        'default_keywords_label' => 'Keywords по умолчанию',
        'default_keywords_description' => 'Этот шаблон будет использован по умолчанию (если поле не заполнено)',
    ],
    'tab' => [
        'easyseo' => 'EasySeo'
    ],
    'settings' => [
        'label' => 'Общее',
        'description' => 'Управление настройками SEO.',
        'category' => 'EasySeo',
    ],
    'component' => [
        'seo_name' => 'Seo',
        'meta_description' => 'Seo',
    ],
    'permission' => [
        'tab' => 'EasySeo',
        'label' => 'Может управлять настройками SEO',
    ]
];
