<?php return [
    'plugin' => [
        'name' => 'EasySeo',
        'description' => 'A simple SEO plugin which works with CMS pages, Static Pages and custom models.',
    ],
    'field' => [
        'meta_title' => 'EasySeo Meta Title',
        'meta_title_placeholder' => 'Around 60 characters for Google',
        'meta_description' => 'EasySeo Meta Description',
        'meta_description_placeholder' => 'Around 120-150 characters for Google',
        'meta_keywords' => 'EasySeo Meta Keywords',
        'meta_keywords_placeholder' => 'There is no official limit',
        'cms_pages' => 'CMS pages',
        'static_pages' => 'Static pages',
        'custom_pages' => 'Custom pages',
        'page' => 'Page',
        'param_label' => 'URL parameter',
        'param_description' => 'Parameter name',
        'empty_option' => '-- Please select --',
        'empty_field' => '-- Please insert --',
        'tab_type' => 'Tab type',
        'path_to_model_label' => 'Path to model',
        'path_to_model_description' => 'Like RainLab\Blog\Models\Post',
        'path_to_controller_label' => 'Path to controller',
        'path_to_controller_description' => 'Like RainLab\Blog\Controllers\Posts',
        'default_title_label' => 'Default Title',
        'default_title_description' => 'This template will be used if field is empty',
        'default_description_label' => 'Default Description',
        'default_description_description' => 'This template will be used if field is empty',
        'default_keywords_label' => 'Default Keywords',
        'default_keywords_description' => 'This template will be used if field is empty',
    ],
    'tab' => [
        'easyseo' => 'EasySeo'
    ],
    'settings' => [
        'label' => 'Common',
        'description' => 'Manage SEO settings.',
        'category' => 'EasySeo',
    ],
    'component' => [
        'seo_name' => 'Seo',
        'meta_description' => 'Seo',
    ],
    'permission' => [
        'tab' => 'EasySeo',
        'label' => 'Can manage Seo settings',
    ]
];
