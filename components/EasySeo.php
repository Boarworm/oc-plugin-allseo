<?php namespace Boarworm\EasySeo\Components;

use Twig;
use Boarworm\EasySeo\Models\Settings;
use Cms\Classes\ComponentBase;

class EasySeo extends ComponentBase
{
    public $meta;

    public function componentDetails()
    {
        return [
            'name' => 'boarworm.easyseo::lang.component.seo_name',
            'description' => 'boarworm.easyseo::lang.component.meta_description'
        ];
    }

    public function onRun()
    {
        $pageType   = $this->getPageType();
        $this->meta = $this->page['meta'] = $this->getMetaData($pageType);
    }

    public function getMetaData($pageType): array
    {
        $meta = [];
        if ($pageType === 'cmsPage') {
            /* if cms version 2 */
            if (class_exists('System')) {
                $meta['meta_title']       = $this->page->meta_title;
                $meta['meta_description'] = $this->page->meta_description;
                $meta['meta_keywords']    = $this->page->meta_keywords;
            } else {
                $meta['meta_title']       = $this->page->viewBag->meta_title;
                $meta['meta_description'] = $this->page->viewBag->meta_description;
                $meta['meta_keywords']    = $this->page->viewBag->meta_keywords;
            }
        }

        if ($pageType === 'meta') {
            $seoRelations = Settings::get('relations');
            foreach ($seoRelations as $seoRelation) {
                $sourcePage = $seoRelation['source_page'];

                if ($this->page->baseFileName === $sourcePage) {
                    $sourceParam = $seoRelation['source_param'];

                    $sourceModel = $seoRelation['item_model'];
                    // get last part of param string
                    $param  = basename($this->param($sourceParam));
                    $record = $sourceModel::where('slug', $param)->first();

                    if ($record) {
                        // apply default values
                        $meta = [
                            'meta_title' => $this->parseTwig($seoRelation['default_title'], $record),
                            'meta_description' => $this->parseTwig($seoRelation['default_description'], $record),
                            'meta_keywords' => $this->parseTwig($seoRelation['default_keywords'], $record),
                        ];
                        if ($record->meta) {
                            if (!empty($meta_title = $this->parseTwig($record->meta->meta_title, $record))) {
                                $meta['meta_title'] = $meta_title;
                            }
                            if (!empty($meta_description = $this->parseTwig($record->meta->meta_description, $record))) {
                                $meta['meta_description'] = $meta_description;
                            }
                            if (!empty($meta_keywords = $this->parseTwig($record->meta->meta_keywords, $record))) {
                                $meta['meta_keywords'] = $meta_keywords;
                            }
                        }

                    }
                }
            }
        }

        if ($pageType === 'staticPage') {
            $pageName                 = $this->page->apiBag['staticPage']['fileName'];
            $staticPage               = \RainLab\Pages\Classes\Page::load($this->controller->getTheme(), $pageName);
            $meta['meta_title']       = $staticPage->viewBag['meta_title'] ?? $this->page->settings['title'];
            $meta['meta_description'] = $staticPage->viewBag['meta_description'] ?? $this->page->settings['title'];
            $meta['meta_keywords']    = $staticPage->viewBag['meta_keywords'] ?? $this->page->settings['title'];
        }

        return $meta;
    }

    protected function parseTwig($originalString, $model): string
    {
        return \Twig::parse($originalString, ['model' => $model]);
    }

    public function getPageType(): string
    {
        if (isset($this->page->apiBag['staticPage'])) {
            return 'staticPage';
        }

        // Check if this page file name in custom models
        if ($seoRelations = Settings::get('relations')) {
            if (in_array($this->page->baseFileName, array_column($seoRelations, 'source_page'), true)) {
                return 'meta';
            }
        }

        return 'cmsPage';
    }
}
