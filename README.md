# EasySeo plugin
A simple SEO plugin which works with CMS pages, Static Pages and custom models.
## Configuration
For StaticPages and Cms pages meta field will be created automatically.
For custom pages in system settings add new configurations. Follow instructions there.
## How to display meta tags
Use the ```EasySeo``` component to display meta tags.
Just put it place where you want to display meta tags.
No additional configuration required.
## Default values
In system setting you can specify default template (for now field template can be specified for only custom models).
## Using Twig
You can use Twig inside meta fields (inside default meta fields or custom models fields only) via ```model``` variable.<br>
For example:<br>
```{{ model.title }} by user "{{ model.user.first_name }}" in category "{{ model.categories[0].name }}"```
