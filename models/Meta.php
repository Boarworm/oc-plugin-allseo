<?php namespace Boarworm\EasySeo\Models;

use Model;

class Meta extends Model
{
  use \October\Rain\Database\Traits\Validation;

  public $table = 'boarworm_easyseo_meta';

  public $timestamps = false;

  public $morphTo = [
    'taggable' => []
  ];

  public $rules = [];

  public $fillable = [
    'meta_title',
    'meta_description',
    'meta_keywords',
    //        'canonical_url',
    //        'redirect_url',
    //        'robot_index',
    //        'robot_follow'
  ];
}
