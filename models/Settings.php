<?php namespace Boarworm\EasySeo\Models;

use Illuminate\Support\Facades\File;
use Model;
use System\Classes\PluginManager;
use Cms\Classes\Page;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'boarworm_easyseo_settings';

    public $settingsFields = 'fields.yaml';

    public function getSourcePageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }
}
